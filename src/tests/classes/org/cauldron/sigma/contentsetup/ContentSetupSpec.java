package org.cauldron.sigma.contentsetup;

import org.quantic.cauldron.pagectx.AbstractPageSpec;

public class ContentSetupSpec extends AbstractPageSpec {

    @Override
    protected void execute() {
        dataform(parent, "content_format");
        tabletool(parent, "file_content");
    }
    
}

