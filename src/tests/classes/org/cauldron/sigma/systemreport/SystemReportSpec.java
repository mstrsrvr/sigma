package org.cauldron.sigma.systemreport;

import org.quantic.cauldron.pagectx.AbstractPageSpec;

public class SystemReportSpec extends AbstractPageSpec {

	@Override
	protected void execute() {
		treetool("content", "system_report.programs.functions");
	}

}

