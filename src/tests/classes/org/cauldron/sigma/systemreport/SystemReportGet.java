package org.cauldron.sigma.systemreport;

import java.util.Map;

import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.facility.AbstractFunction;
import org.quantic.cauldron.facility.Facility;
import org.quantic.cauldron.facility.FunctionConfig;
import org.quantic.cauldron.facility.FunctionDetails;
import org.quantic.cauldron.facility.Session;

public class SystemReportGet extends AbstractFunction {

	public SystemReportGet() {
		super("system_report_get");
	}
	
	private final String concat(String parent, String name, String sep) {
		return new StringBuilder(parent).append(sep).append(name).toString();
	}

	@Override
	protected void config(FunctionConfig config) {
		config.output("sigma.system_report");
	}

	@Override
	protected void execute(Session session) throws Exception {
		Facility facility;
		FunctionDetails functiondet;
		DataContextEntry programs, functions, function, params, inputs;
		DataContextEntry input, output;
		Map<String, String> fdinputs;
		Context context = session.context();
		DataContextEntry report = session.output();

		programs = report.instance("programs");
		functions = programs.instance("functions");
		functions.set("functions");
		for (String key : context.getFacilities()) {
			facility = context.getFacility(key);
			for (String fnkey : facility.getFunctions()) {
				function = functions.instance();
				function.set("name", concat(key, fnkey, "."));
				params = function.instance("parameters");
				inputs = params.instance("inputs");
				inputs.set("inputs");
				functiondet = facility.getFunctionDetails(fnkey);
				fdinputs = functiondet.getInputs();
				for (String fdkey : fdinputs.keySet()) {
					input = inputs.instance();
					input.set("name", concat(fdinputs.get(fdkey), fdkey, ": "));
				}
				output = params.instance("output");
				output.set("output");
				output.instance().set("name", functiondet.output);
			}
		}
	}
	
}