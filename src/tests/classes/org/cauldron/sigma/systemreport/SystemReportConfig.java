package org.cauldron.sigma.systemreport;

import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.pagectx.AbstractPageConfig;
import org.quantic.cauldron.pagectx.PageContext;
import org.quantic.cauldron.pagectx.ToolData;

public class SystemReportConfig extends AbstractPageConfig<Context> {

	@Override
	protected void execute(PageContext pagectx, Context context) {
		ToolData functions = getTool("system_report.programs.functions");
		
		functions.facility = "sigma";
		functions.model = "functions";
	}

}
