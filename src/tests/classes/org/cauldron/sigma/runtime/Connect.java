package org.cauldron.sigma.runtime;

import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.facility.AbstractFunction;
import org.quantic.cauldron.facility.FunctionConfig;
import org.quantic.cauldron.facility.Session;

public class Connect extends AbstractFunction {

	public Connect() {
		super("connect");
	}
	
	@Override
	protected final void config(FunctionConfig config) {
		config.input("sigma.logon", "logon");
		config.output("sigma.connection_ok");
	}
	
	@Override
	protected final void execute(Session session) throws Exception {
		DataContextEntry login = session.geto("logon");
		DataContextEntry reqack = session.output();
		
		reqack.set("user_request", login);
			
//		session.error("erro de conexão");
	}
}
