package org.cauldron.sigma.runtime.formats;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.cauldron.sigma.Calculations;
import org.cauldron.sigma.runtime.FormatData;
import org.odftoolkit.simple.SpreadsheetDocument;
import org.odftoolkit.simple.table.Cell;
import org.odftoolkit.simple.table.Row;
import org.odftoolkit.simple.table.Table;

public class ODFContent {
    private static final Map<String, ODFCellHandler> celltypes;
    
    static {
        celltypes = new HashMap<>();
        celltypes.put("boolean", (cell)->{return cell.getBooleanValue();});
        celltypes.put("currency", (cell)->{return cell.getCurrencyValue();});
        celltypes.put("date", (cell)->{return cell.getDateValue();});
        celltypes.put("float", (cell)->{return cell.getDoubleValue();});
        celltypes.put("percentage", (cell)->{return cell.getPercentageValue();});
        celltypes.put("string", (cell)->{return cell.getStringValue();});
        celltypes.put("time", (cell)->{return cell.getTimeValue();});
    }
    
    public static final FormatData get(Calculations context, InputStream is)
            throws Exception {
        Cell cell;
        String type;
        ODFCellHandler cellh;
        String[][] lines;
        FormatData data;
        int maxc = 0, j = 0;
        SpreadsheetDocument document = SpreadsheetDocument.loadDocument(is);
        
        for (Table table : document.getTableList()) {
            lines = new String[table.getRowCount()][];
            for (Row row : table.getRowList()) {
                lines[j] = new String[row.getCellCount()];
                if (lines[j].length > maxc)
                    maxc = lines[j].length;
                for (int i = 0; i < lines[j].length; i++) {
                    cell = row.getCellByIndex(i);
                    type = cell.getValueType();
                    if ((cellh = celltypes.get(type)) != null)
                        lines[j][i] = cellh.get(cell).toString();
                }
                j++;
            }
            data = context.getGridObjects(lines, maxc);
            data.page = "customer-content-upload-2";
            return data;
        }
        return null;
    }
    
}

interface ODFCellHandler {
    
    public abstract Object get(Cell cell);
}
