package org.cauldron.sigma.runtime.formats;

import java.util.HashMap;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.cauldron.sigma.Calculations;
import org.cauldron.sigma.runtime.FormatData;

public class WorkBookContent {
    private static final Map<CellType, POICellHandler> celltypes;

    static {
        celltypes = new HashMap<>();
        celltypes.put(CellType.STRING,
                (cell)->{return cell.getStringCellValue();});
        celltypes.put(CellType.NUMERIC,
                (cell)->{return cell.getNumericCellValue();});
        celltypes.put(CellType.BOOLEAN,
                (cell)->{return cell.getBooleanCellValue();});
    }
    
    public static final FormatData get(Calculations context, Workbook workbook)
    {
        Row row;
        Cell cell;
        POICellHandler ctypeh;
        FormatData data;
        Sheet sheet = workbook.getSheetAt(0);
        String[][] lines = new String[sheet.getLastRowNum() + 1][];
        int maxc = 0;
        
        for (int j = 0; j < lines.length; j++) {
            row = sheet.getRow(j);
            lines[j] = new String[row.getLastCellNum()];
            if (lines[j].length > maxc)
                maxc = lines[j].length;
            for (int i = 0; i < lines[j].length; i++) {
                cell = row.getCell(i);
                ctypeh = celltypes.get(cell.getCellTypeEnum());
                if (ctypeh != null)
                    lines[j][i] = ctypeh.get(cell).toString();
            }
        }
        
        data = context.getGridObjects(lines, maxc);
        data.page = "customer-content-upload-2";
        return data;
    }
    
//    private static final ExtendedObject[] getGridLayout(FormatData data) {
//        String name;
//        int j = 0;
//        DocumentModelItem[] mitems = data.model.getItens();
//        ExtendedObject[] objects = new ExtendedObject[mitems.length];
//        
//        for (DocumentModelItem mitem : mitems) {
//            objects[j] = new ExtendedObject(layoutformat);
//            objects[j].set("NAME", name = mitem.getName());
//            objects[j++].set("NAME_EDIT", name);
//        }
//        
//        return objects;
//    }
//    
//    private static final FormatData getGridObjects(
//            String[][] lines, ExtendedObject[] columns, String field) {
//        String[] values = new String[columns.length];
//        for (int i = 0; i < columns.length; i++)
//            values[i] = columns[i].getst(field);
//        return getGridObjects(lines, values);
//    }
    
}