package org.cauldron.sigma.runtime;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.cauldron.sigma.Calculations;
import org.cauldron.sigma.runtime.formats.DefaultContent;
import org.cauldron.sigma.runtime.formats.ODFContent;
import org.cauldron.sigma.runtime.formats.WorkBookContent;
import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.datamodel.DataContext;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.facility.AbstractFunction;
import org.quantic.cauldron.facility.FunctionConfig;
import org.quantic.cauldron.facility.Session;

public class Upload extends AbstractFunction {
    private static final String ODF =
        "application/vnd.oasis.opendocument.spreadsheet";
    private static final String XLS =
        "application/vnd.ms-excel";
    private static final String XLSX =
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    private Map<String, FormatHandler> formats;

	public Upload() {
		super("upload");
//        formats = new HashMap<>();
//        formats.put(ODF, (context, is)->{
//            return ODFContent.get(context, is);});
//        formats.put(XLS, (context, is)->{
//            return WorkBookContent.get(context, new HSSFWorkbook(is));});
//        formats.put(XLSX, (context, is)->{
//            return WorkBookContent.get(context, new XSSFWorkbook(is));});
//        formats.put("default", (context, is)->{
//            return DefaultContent.get(context, is);});
	}

	@Override
	protected void config(FunctionConfig config) { }

	@Override
	protected void execute(Session session) throws Exception {
//		DataContextEntry project;
//		Context context = session.context();
//		DataContext datactx = context.datactx();
//		byte[] bcontent = datactx.get("context.main.file").get();
//		String contenttype = (String)context.getPage().
//				instance("context.main.file").values.get("content-type");
//        InputStream is = new ByteArrayInputStream(bcontent);
//        FormatHandler format = formats.
//        		getOrDefault(contenttype, formats.get("default"));
//        FormatData data = Calculations.extract(context, is);
//        
//        Calculations.getGridLayout(data);
//        project = datactx.instance("project", "project");
//		project.set("name", datactx.get("context.main.name"));
//		project.set("text", datactx.get("context.main.text"));
	}
	
}
