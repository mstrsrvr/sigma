package org.cauldron.sigma.runtime;

import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.datamodel.DataContextEntry;

public class FormatData {
	public Context context;
	public DataContextEntry objects;
	public String page;
}
