package org.cauldron.sigma.contentupload;

import org.quantic.cauldron.pagectx.AbstractPageSpec;

public class ContentUploadSpec extends AbstractPageSpec {

	@Override
	protected void execute() {
		dataform("content", "context.main");
		button("content", "upload");
	}
	
}

