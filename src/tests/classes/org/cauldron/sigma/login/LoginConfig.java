package org.cauldron.sigma.login;

import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.pagectx.AbstractPageConfig;
import org.quantic.cauldron.pagectx.Messages;
import org.quantic.cauldron.pagectx.PageContext;
import org.quantic.cauldron.pagectx.PageSpecItem.TYPES;
import org.quantic.cauldron.pagectx.ToolData;

public class LoginConfig extends AbstractPageConfig<Context> {

	@Override
	protected void execute(PageContext pagectx, Context context) {
		ToolData data;
		Messages messages;
		
		pagectx.setTitle("Quantic Software | Login");
		
		data = getTool("fullcontent");
		data.style = "row full-height";
		
		data = getTool("sideimg");
		data.style = "col-sm-8 fundo";
		
		data = getTool("logo");
		data.style = "img-fluid logo-quantic";
		data.attributes.put("src", "imgs/qs-branco.png");
		data.attributes.put("alt", "logo-quantic");
		data.attributes.put("width", "500");
		data.attributes.put("height", "500");
		data.tag = "img";
		
		data = getTool("loginside");
		data.style = "col-sm-4";
		
		data = getTool("loginform");
		data.style = "container";
		
		data = getTool("locale");
		data.style = "row";
		
		data = getTool("localeleft");
		data.style = "col-6 esquerda";
		
		data = getTool("localeright");
		data.style = "col-6 direita";

		data = getTool("localeextcnt");
		data.style = "form-row";
		
		data = getTool("localeinnercnt");
		data.style = "col-xs-3 ml-auto mt-3 mb-3";
		
		data = getTool("localelabel");
		data.attributes.put("for", "login.locale");
		data.tag = "label";
		data.text = "login.label";
		
		data = getTool("login.locale");
		data.style = "form-control";
		data.values.put("pt_BR", "Português");
		data.values.put("en_US", "English");
		data.values.put("es_ES", "Español");
		data.current = "pt_BR";
		
		data = getTool("mobilelogo");
		data.style = "row logo-mobile";

		data = getTool("logincnt");
		data.style = "formulario-login";
		
		data = getTool("login");
		data.model = "logon";
		data.facility = "sigma";
		data.styles.put("item", "form-group text-center");
		data.styles.put("item_text", "control-label");
		data.styles.put("item_input", "form-control");
		data.internallabel = true;
		data.instance("user_text").tag = "label";
		data.instance("secret_text").tag = "label";
		data.instance(TYPES.TEXT_FIELD, "secret").secret = true;
		data.instance(TYPES.TEXT_FIELD, "locale").invisible = true;
		
		data = getTool("conn_container");
		data.style = "form-row mt-5";
		
		data = getTool("connect");
		data.style = "btn btn-primary btn-lg btn-block";
		data.submit = true;
		
		messages = pagectx.getMessages("pt_BR");
		messages.put("connect", "Conectar");
		messages.put("login.label", "Selecionar idioma");
		messages.put("login.password", "Senha");
		messages.put("login.username", "Usuário");
	}
	
}
