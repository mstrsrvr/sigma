package org.cauldron.sigma.login;

import org.quantic.cauldron.pagectx.AbstractPageSpec;

public class LoginSpec extends AbstractPageSpec {

	@Override
	protected void execute() {
		container("fullcontent", "sideimg");
		container("sideimg", "logo");
		container("fullcontent", "loginside");
		container("loginside", "loginform");
		container("loginform", "locale");
		container("locale", "localeleft");
		container("locale", "localeright");
		container("localeright", "localeextcnt");
		container("localeextcnt", "localeinnercnt");
		text("localeinnercnt", "localelabel");
		listbox("localeinnercnt", "login.locale");
		container("loginform", "mobilelogo");
		container("loginform", "logincnt");
		dataform("logincnt", "login");
		container("logincnt", "conn_container");
		button("conn_container", "connect");
	}
	
}

