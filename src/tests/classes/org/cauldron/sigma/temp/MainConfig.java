package org.cauldron.sigma.temp;

import org.cauldron.sigma.Calculations;
import org.quantic.cauldron.application.Context;
import org.quantic.cauldron.pagectx.AbstractPageConfig;
import org.quantic.cauldron.pagectx.Messages;
import org.quantic.cauldron.pagectx.PageContext;
import org.quantic.cauldron.pagectx.ToolData;

public class MainConfig extends AbstractPageConfig<Context> {

	@Override
	protected void execute(PageContext pagectx, Context context) {
//		ToolData data;
//		Messages messages;
//		PageContext pagectx = context.getPage("main");
//		
//		pagectx.setResponseComponent("fullcontent");
//		pagectx.getHead().putAll(SigmaContext.H_ITEMS);
//		
//		data = getTool("main");
//		data.attributes.put("style", "width:100%;height:100%;");
//		
//		data = getTool("fullcontent");
//		data.attributes.put("font-family", "'Roboto', sans-serif;");
//		
//		data = getTool("message");
//		data.tag = "div";
//		data.style = "text-center full fixed-bottom";
//		data.styles.put(
//				"error", "error-message text-center full fixed-bottom");
//		data.styles.put(
//				"status", "status-message text-center full fixed-bottom");
//		data.styles.put(
//				"warning", "warning-message text-center full fixed-bottom");
//		
//		data = getTool("navbar");
//		data.style = "navbar fixed-top navbar-expand-lg navbar-dark bg-primary";
//		data.tag = "nav";
//		
//		data = getTool("navbarcontent");
//		data.style = "container";
//		
//		data = getTool("titlebar");
//		data.attributes.put("style", "padding-top:4rem;text-align:center;");
//		data.tag = "h1";
//		
//		data = getTool("data");
//		data.styles.put("button_bar", "nav nav-pills mb-4");
//		data.styles.put("button_bar_item", "nav-item");
//		data.styles.put("active_button", "nav-link active");
//		data.styles.put("deactive_button", "nav-link");
//		data.styles.put("panes_area", "tab-content mb-5");
//		data.styles.put("active_pane", "tab-pane fade active show");
//		data.styles.put("deactive_pane", "tab-pane fade");
//		
//		data = getTool("logo");
//		data.style = "navbar-brand mr-5";
//		data.source = "login.html";
//		data.absolute = true;
//		
//		data = getTool("logoimg");
//		data.attributes.put("src", "./imgs/qs-branco.png");
//		data.attributes.put("alt", "logo-quantic-software");
//		data.attributes.put("width", "120");
//		data.tag = "img";
//		
//		data = getTool("content");
//		data.style = "container";
//
//		data = getTool("materials_data");
//		data.attributes.put("style", "overflow: auto");
//		
//		data = getTool("materials");
//		data.facility = "taxes-integration";
//		data.model = "materials";
//		data.style = "table table-striped";
//		
//		data = getTool("st_taxes_data");
//		data.attributes.put("style", "overflow: auto");
//		
//		data = getTool("st_taxes");
//		data.facility = "taxes-integration";
//		data.model = "st_taxes";
//		data.style = "table table-striped";
//
//		data = getTool("cust_taxes_data");
//		data.attributes.put("style", "overflow: auto");
//		
//		data = getTool("cust_taxes");
//		data.facility = "taxes-integration";
//		data.model = "cust_taxes";
//		data.style = "table table-striped";
//		
//		data = getTool("taxes_view_cnt");
//		data.attributes.put("style", "overflow: auto");
//		
//		data = getTool("teste");
//		data.style = "btn btn-primary";
//		
//		data = getTool("taxes_view");
//		data.facility = "taxes-integration";
//		data.model = "taxes_view";
//		data.style = "table table-striped";
//		data.mark = "mark";
//		data.factory = new TaxesViewTableFactory();
//		
//		messages = pagectx.getMessages("pt_BR");
//		messages.put("adjusted_rate", "Alíq.Ajst.");
//		messages.put("calculation", "Regra");
//		messages.put("cust_taxes_data", "Alíq.Cliente");
//		messages.put("fcp_rate", "Alíq.FCP");
//		messages.put("group", "Grupo");
//		messages.put("icms_base_r", "BaseICMS");
//		messages.put("id", "Id.ST");
//		messages.put("internal_rate", "Alíq.Int.");
//		messages.put("interstate_mi_rate", "Alíq.IE.MI.");
//		messages.put("interstate_rate", "Alíq.IE.");
//		messages.put("mark", "");
//		messages.put("materials_data", "Materiais");
//		messages.put("mi_adjusted_rate", "Alíq.MI.Ajst.");
//		messages.put("ncm", "NCM");
//		messages.put("original_rate", "Alíq.Orig.");
//		messages.put("preview_icms_base_r", "B.ICMSPrevista");
//		messages.put("preview_rate", "Alíq.Prevista");
//		messages.put("rate", "Alíq.");
//		messages.put("source", "UF.Orig.");
//		messages.put("st_taxes_data", "ST");
//		messages.put("target", "UF.Dest.");
//		messages.put("target_rate", "Alíq.Dest.");
//		messages.put("taxes_view_data", "Previsão de alíquotas");
//		messages.put("valid_from", "Vál.De");
//		messages.put("valid_to", "Vál.Até");
	}
	
}
