package org.cauldron.sigma.temp;

import org.quantic.cauldron.pagectx.AbstractPageSpec;

public class MainSpec extends AbstractPageSpec {

	@Override
	protected void execute() {
		form("main");
		container("main", "fullcontent");
		message("fullcontent", "message");
		container("fullcontent", "navbar");
		container("navbar", "navbarcontent");
		link("navbarcontent", "logo");
		container("logo", "logoimg");
		container("fullcontent", "content");
		container("fullcontent", "status");
		text("content", "titlebar");
		tabbedpane("content", "data");
		tabbedpaneitem("data", "materials_data");
		tabletool("materials_data", "materials");
		tabbedpaneitem("data", "st_taxes_data");
		tabletool("st_taxes_data", "st_taxes");
		tabbedpaneitem("data", "cust_taxes_data");
		tabletool("cust_taxes_data", "cust_taxes");
		tabbedpaneitem("data", "taxes_view_data");
		button("taxes_view_data", "teste");
		container("taxes_view_data", "taxes_view_cnt");
		tabletool("taxes_view_cnt", "taxes_view");
	}
	
}

