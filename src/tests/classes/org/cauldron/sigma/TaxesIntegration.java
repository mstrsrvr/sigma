package org.cauldron.sigma;

import java.util.Calendar;
import java.util.Date;

import org.cauldron.sigma.runtime.Connect;
import org.quantic.cauldron.datamodel.DataContext;
import org.quantic.cauldron.datamodel.DataContextEntry;
import org.quantic.cauldron.datamodel.TypeContext;
import org.quantic.cauldron.datamodel.TypeContextEntry;
import org.quantic.cauldron.facility.AbstractFacility;
import org.quantic.cauldron.facility.FunctionConfig;

public class TaxesIntegration {
	
	private final void custtax(DataContextEntry custtaxes, int group,
			String ncm, String source, String target, double base, double rate)
	{
		Date validfrom, validto;
		DataContextEntry custtax = custtaxes.instance();
		Calendar cvalidfrom = Calendar.getInstance();
		Calendar cvalidto = Calendar.getInstance();

		cvalidfrom.set(2019, 01, 01);
		cvalidto.set(2999, 12, 31);
		validfrom = cvalidfrom.getTime();
		validto = cvalidto.getTime();
		
		custtax.set("group", group);
		custtax.set("ncm", ncm);
		custtax.set("source", source);
		custtax.set("target", target);
		custtax.set("valid_from", validfrom);
		custtax.set("valid_to", validto);
		custtax.set("icms_base_r", base);
		custtax.set("rate", rate);
	}
	
//	@Override
//	protected void init(Calculations context) {
////		TypeContextEntry materialt, materialst, sttaxt, sttaxest, custtaxt;
////		TypeContextEntry custtaxest, taxviewt, taxesviewt, contextt, maint;
////		TypeContextEntry contentlinet, columnformatt, layoutformatt;
//		TypeContextEntry usrreqt, useropreqt, numobjt, reqackt;//projectt;
////		DataContextEntry materials, sttaxes, sttax, custtaxes;
////		DataContextEntry taxesview;
//		TypeContext typectx = context.typectx();
////		DataContext datactx = context.datactx();
////		
//		numobjt = typectx.define("number_object", "type");
//		numobjt.addst("type");
//		numobjt.addl("current");
//		
//		usrreqt = typectx.define("user_access_request");
//		usrreqt.addst("username");
//		usrreqt.addst("password");
//		usrreqt.addst("locale");
////		
////		useropreqt = typectx.define("user_operation_request");
//		
//		reqackt = typectx.define("access_request_accepted", "id");
//		reqackt.addl("id");
//		reqackt.add(usrreqt, "user_request");
//		reqackt.autoGenKey();
//		reqackt.persist();
////		
////		projectt = typectx.define("project");
////		projectt.addst("name");
////		projectt.addst("user_id");
////		projectt.addst("text");
////		
////		/*
////		 * layout_format
////		 */
////		columnformatt = typectx.define("column_format");
////		columnformatt.addst("name");
////		columnformatt.addst("name_edit");
////		columnformatt.addbl("delete");
////		
////		layoutformatt = typectx.define(columnformatt, "layout_format", null);
////		layoutformatt.array();
////		
////		/*
////		 * main
////		 */
////		maint = typectx.define("main");
////		maint.copy(projectt);
////		maint.addst("file");
////		
////		contentlinet = typectx.define("content_line");
////		contentlinet.addst("line");
////		
////		/*
////		 * content_model
////		 */
////		context.contentmodel =
////				typectx.define(contentlinet, "content_model", null);
////		context.contentmodel.array();
////		
////		/*
////		 * content_format
////		 */
////		context.contentformat =
////				typectx.define("content_format");
////		context.contentformat.addst("separator");
////		
////		contextt = typectx.define("context");
////		contextt.adds(maint, "main");
////		
////		datactx.instance(contextt, "context");
////		
////		materialt = typectx.define("material", "id");
////		materialt.addst("id");
////		materialt.addst("text");
////		materialt.addst("ncm");
////		
////		materialst = typectx.define(materialt, "materials", null);
////		materialst.array();
////		
////		materials = datactx.instance(materialst, "materials");
////		material(materials, "MAT01", "Material 01", "85043300");
////		material(materials, "MAT02", "Material 02", "85043400");
////		material(materials, "MAT03", "Material 03", "85045000");
////		
////		sttaxt = typectx.define("st_tax", "id");
////		sttaxt.addst("id");
////		sttaxt.addst("ncm");
////		sttaxt.addst("source");
////		sttaxt.addst("target");
////		sttaxt.addd("target_rate");
////		sttaxt.addd("interstate_rate");
////		sttaxt.addd("interstate_mi_rate");
////		sttaxt.addd("internal_rate");
////		sttaxt.addd("fcp_rate");
////		sttaxt.addd("original_rate");
////		sttaxt.addd("adjusted_rate");
////		sttaxt.addd("mi_adjusted_rate");
////		
////		sttaxest = typectx.define(sttaxt, "st_taxes", null);
////		sttaxest.array();
////		
////		sttaxes = datactx.instance(sttaxest, "st_taxes");
////		sttax = sttaxes.instance();
////		sttax.set("id", 8140);
////		sttax.set("ncm", "8504");
////		sttax.set("source", "SP");
////		sttax.set("target", "MG");
////		sttax.set("target_rate",18.000);
////		sttax.set("interstate_rate", 12.000);
////		sttax.set("interstate_mi_rate", 4.000);
////		sttax.set("internal_rate", 0);
////		sttax.set("fcp_rate", 0);
////		sttax.set("original_rate", 50.000);
////		sttax.set("adjusted_rate", 60.980);
////		sttax.set("mi_adjusted_rate", 75.610);
////		
////		custtaxt = typectx.define("cust_tax");
////		custtaxt.addi("group");
////		custtaxt.addst("ncm");
////		custtaxt.addst("source");
////		custtaxt.addst("target");
////		custtaxt.adddt("valid_from");
////		custtaxt.adddt("valid_to");
////		custtaxt.addd("icms_base_r");
////		custtaxt.addd("rate");
////		
////		custtaxest = typectx.define(custtaxt, "cust_taxes", null);
////		custtaxest.array();
////		
////		custtaxes = datactx.instance(custtaxest, "cust_taxes");
////		custtax(custtaxes, 23, "85043300", "SP", "MG", 0.000, 14.000);
////		custtax(custtaxes, 23, "85043400", "SP", "MG", 0.000, 14.000);
////		custtax(custtaxes, 23, "85045000", "SP", "MG", 0.000, 14.000);
////		custtax(custtaxes, 25, "85043300", "SP", "MG", 0.000, 0.000);
////		custtax(custtaxes, 25, "85043400", "SP", "MG", 0.000, 0.000);
////		custtax(custtaxes, 25, "85045000", "SP", "MG", 0.000, 0.000);
////		
////		taxviewt = typectx.define("tax_view");
////		taxviewt.addbl("mark");
////		taxviewt.addst("calculation");
////		taxviewt.copy(sttaxt);
////		taxviewt.copy(custtaxt);
////		taxviewt.addd("preview_icms_base_r");
////		taxviewt.addd("preview_rate");
////		
////		taxesviewt = typectx.define(taxviewt, "taxes_view", null);
////		taxesviewt.array();
////		
////		taxesview = datactx.instance(taxesviewt, "taxes_view");
////		context.updateTaxesView(taxesview, custtaxes, sttaxes);
//	}
	
	private final void material(
			DataContextEntry materials, String id, String text, String ncm) {
		DataContextEntry material = materials.instance();
		material.set("id", id);
		material.set("text", text);
		material.set("ncm", ncm);
	}
}
