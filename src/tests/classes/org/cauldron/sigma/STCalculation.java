package org.cauldron.sigma;

import org.quantic.cauldron.datamodel.DataContextEntry;

public interface STCalculation {
	
	public abstract void execute(DataContextEntry taxview);
	
}