window.addEventListener('load', function(event) { sigma_init(); });

function sigma_control_add(control, servret) {
    let form = control.getAttribute('data-sigma-form');
    let action = control.getAttribute('data-sigma-action');
    let updtarea = control.getAttribute('data-sigma-updtarea');
    sigma_listener_add(
        'click', updtarea, 'index.html', control.id, form, action, servret);
}


function sigma_init() {
    let buttons = document.getElementsByTagName('button');
    for (let button of buttons)
        sigma_control_add(button, true);
    
    let links = document.getElementsByTagName('a');
    for (let link of links)
        sigma_control_add(link, false);
}
        
function sigma_back_listener_add(updtarea, target, formname) {
    window.onpopstate = function(event) {
        sigma_request(event, updtarea, target, formname, 'back', true);
    }
}

function sigma_response(servret, updtarea, respcontent) {
    if (!servret)
        return;
    if (respcontent.readyState == 4 && respcontent.status == 200) {
        document.getElementById(updtarea).innerHTML = respcontent.responseText;
        sigma_init();
    }
}

function sigma_request(event, updtarea, target, formname, action, servret) {
    event.preventDefault();
    
    let form = new FormData();
    let inputs = document.getElementsByTagName('input');
    for (let input of inputs) {
       switch (input.type) {
       case "checkbox":
          form.append(input.id, input.checked);
          break;
       case "file":
          form.append(input.id, input.files[0]);
          break;
       default:
          form.append(input.id, input.value);
          break;
       }
    }
    inputs = document.getElementsByTagName('select');
    for (input of inputs)
       form.append(input.id, input.value);
    form.append('action', action);
    
    let request = new XMLHttpRequest();
    request.onreadystatechange = function() {
        sigma_response(servret, updtarea, this);
    };
    request.open('POST', target, /* async = */ false);
    request.send(form);
}

function sigma_listener_add(
        event, updtarea, target, cntrlnm, formname, action, servret) {
    let control = document.getElementById(cntrlnm);
    control.addEventListener(event, function(event) {
        sigma_request(event, updtarea, target, formname, action, servret);
    });
}
